
// відправка введених даних на сервер
function sendDataToTheServer (){
    const createButt = document.querySelector('.create_visit_butt');
    const visitForm = document.querySelector('form');   

    const doctorsSelect  = document.getElementById('doctors-select');
    const urgencySelect = document.getElementById('urgency-select'); 

    doctorsSelect.addEventListener('change', ()=>{
        const selectedDoctor = doctorsSelect.options[doctorsSelect.selectedIndex].value;
        createListBasedOnTheDoctor(selectedDoctor);
    })


    createButt.addEventListener('click', () => {

        //перетворення отриманих даних в новий обєкт 
        const formData = new FormData(visitForm);

        // додавання обраного значення з випадаючого списку до створеного обєкту formData
        const selectedDoctor = doctorsSelect.options[doctorsSelect.selectedIndex].value;
        formData.append('doctor', selectedDoctor);
           
        const selectedUrgency = urgencySelect.options[urgencySelect.selectedIndex].value;
        formData.append('urgency', selectedUrgency);
        
        fetch("https://ajax.test-danit.com/api/v2/cards", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer 485191dd-2ad5-4fd7-a409-ed2819731db9',
            },
            body: JSON.stringify(Object.fromEntries(formData)),
        })
        .then(response => response.json())
        .then(data =>{
            console.log(data);
        })
        .catch(error => {
            console.error('An error occurred:', error);
          });
    })
    
};

function createListBasedOnTheDoctor(doctor){
    const allDoctors = document.querySelectorAll('.cardiologist, .dentist, .therapist');
    allDoctors.forEach(e=>{
        // перевірка за класом 
        e.classList.contains(doctor) ? e.style.display = 'block' : e.style.display = 'none';
    })
};


// цей код мені потрібен тільки для перевірки, в кінці я його видалю 
// отримання даних
async function getDataFromTheServer(){
   try{
    const response = fetch("https://ajax.test-danit.com/api/v2/cards", {
        method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer 485191dd-2ad5-4fd7-a409-ed2819731db9',
            },
    });
    if(!response.ok){
        throw new Error (`Request failed with status: ${response.status}`);
    }

    const data = (await response).json();
    console.log(data);
   }catch (err){
    console.error('An error occurred:', err);
   }
};

sendDataToTheServer();
// getDataFromTheServer();